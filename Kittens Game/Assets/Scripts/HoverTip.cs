using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoverTip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string tipToShow;
    private float timeToWait = 0.5f;

    private string StoredCatnip;
    private string CatFieldCost;
    private string StoredWood;
    private string HutCost;
    private string AssignedWoodcutters;

    private void Update()
    {
        StoredCatnip = ButtonBehavior.instance.StoredCatnip.ToString("F2");
        CatFieldCost = ButtonBehavior.instance.CatnipFieldCost.ToString("F2");
        StoredWood = ButtonBehavior.instance.StoredWood.ToString("F2");
        HutCost = ButtonBehavior.instance.HutCost.ToString("F2");
        AssignedWoodcutters = ButtonBehavior.instance.AssignedWoodcut.ToString();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        StopAllCoroutines();
        StartCoroutine(StartTimer());
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StopAllCoroutines();
        HoverTipManager.OnMouseLoseFocus();
    }

    private void ShowMessage()
    {

        if (this.gameObject.name == "btn_Gather")
        {
            HoverTipManager.OnMouseHover("Gather some catnip in the forest", Input.mousePosition);
        }
        if (this.gameObject.name == "btn_Refine")
        {
            HoverTipManager.OnMouseHover("Refine catnip into catnip wood\n" + "Catnip: 50", Input.mousePosition);
        }
        if (this.gameObject.name == "btn_PurchaseCatField")
        {
            HoverTipManager.OnMouseHover("Plant some catnip to grow in the village.\nFields have a +50% production in Spring and a -75% production in Winter.\nCatnip: " + StoredCatnip + "/" + CatFieldCost + "\nEffects:\nCatnip production: 0.63/s", Input.mousePosition);
        }
        if (this.gameObject.name == "btn_PurchaseHut")
        {
            HoverTipManager.OnMouseHover("Build a Hut (each has a space for 2 kittens).\nKittens need catnip to eat, or they will die.\nEvery kitten consumes about 4 catnip/s\nWood: "+StoredWood+"/"+HutCost+"\nEffects:\nKittens:2", Input.mousePosition);
        }
        if (this.gameObject.name == "IconWoodCutter")
        {
            HoverTipManager.OnMouseHover("Woodcutter (" +AssignedWoodcutters+ ")\n+0.09 wood per second\nEffects:\nWood:0.09/s", Input.mousePosition);
        }
        if (this.gameObject.name == "btn_ClearAll")
        {
            HoverTipManager.OnMouseHover("Clear all jobs", Input.mousePosition);
        }
        

    }

    private IEnumerator StartTimer()
    {
        yield return new WaitForSeconds(timeToWait);

        ShowMessage();
    }
}
