using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using Unity.VisualScripting;

public class Window : MonoBehaviour
{

    private 

    // Start is called before the first frame update
    void Start()
    {
        //transform.Find("btn_Bonfire").GetComponent<Button_UI>().MouseOverOnceFunc = () => TooltipScreenSpaceUI.ShowTooltip_Static("Gather");
        //transform.Find("btn_Bonfire").GetComponent<Button_UI>().MouseOutOnceFunc = () => TooltipScreenSpaceUI.HideTooltip_Static();

        transform.Find("btn_Gather").GetComponent<Button_UI>().MouseOverOnceFunc = () => TooltipScreenSpaceUI.ShowTooltip_Static("Gather");
        transform.Find("btn_Gather").GetComponent<Button_UI>().MouseOutOnceFunc = () => TooltipScreenSpaceUI.HideTooltip_Static();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
