using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CodeMonkey.Utils;

public class ButtonBehavior : MonoBehaviour
{

    public static ButtonBehavior instance;

    //Catnip

    //texts
    public TextMeshProUGUI StoredCatnipText;
    public TextMeshProUGUI CatnipFieldText;
    public TextMeshProUGUI CatnipIncomeText;
    public string CatnipFieldCostText;

    //gameobjects
    public GameObject CatnipResourceBar;
    public GameObject CatnipField;

    //buttons
    public Button PurchaseCatnipFieldButton;
    public Button RefineCatnipButton;
    public Button SellCatnipFieldButton;

    //Variables
    public float StoredCatnip = 0;
    public float CatnipFieldCost = 10;
    float CatnipIncome;
    int CatnipFieldNumber = 0;
    public int GatherCatnipValue;
    float CatFieldIncome = 0.63f;

    //Wood

    //texts
    public TextMeshProUGUI StoreWoodText;
    public TextMeshProUGUI WoodIncomeText;

    //gameobjects
    public GameObject WoodResourceBar;

    //Variables
    public float StoredWood = 0;
    float WoodIncome = 0;
    public float RefineCost = 100;

    //Hut

    //texts
    public TextMeshProUGUI HutText;

    //gameobjects
    public GameObject Hut;

    //buttons
    public Button PurchaseHutButton;
    public Button SellHutButton;

    //Variables
    public float HutCost =5;
    int HutNumber = 0;

    //SECTIONS
    public GameObject Bonfire;
    public GameObject Village;
    //Section Buttons
    public GameObject VillageBtn;

    //Kittens
    int AllKittens = 0;
    int FreeKittens = 0;
    public TextMeshProUGUI KittensText;

    //Woodcutter

    //texts
    public TextMeshProUGUI AssignedWoodcutText;

    //buttons
    public Button AddWoodcutButton;
    public Button AddAllWoodcutButton;
    public Button RemoveWoodcutButton;
    public Button RemoveAllWoodcutButton;

    //variables
    public int AssignedWoodcut;
    float WoodCutterIncome = 0.09f;

    private void Start()
    {
        instance = this;
    }
    private void Update()
    {
        //Catnip
        StoredCatnipText.text = StoredCatnip.ToString("F2");
        if(StoredCatnip >= CatnipFieldCost )
        {
            PurchaseCatnipFieldButton.interactable = true;
        }
        if(StoredCatnip < CatnipFieldCost )
        {
            PurchaseCatnipFieldButton.interactable= false;
        }
        CatnipIncomeText.text = CatnipIncome.ToString("F2") + "/s";
        CatnipIncome = CatnipFieldNumber * CatFieldIncome;
        StoredCatnip += CatnipIncome * Time.deltaTime;

        CatnipFieldCostText = CatnipFieldCost.ToString();

        //Wood
        StoreWoodText.text = StoredWood.ToString("F2");
        
        if(StoredCatnip >= RefineCost)
        {
            RefineCatnipButton.interactable = true;
        }
        if(StoredCatnip < RefineCost )
        {
            RefineCatnipButton.interactable = false;
        }

        WoodIncome = AssignedWoodcut * WoodCutterIncome;
        WoodIncomeText.text = WoodIncome.ToString("F2") + "/s";
        StoredWood += WoodIncome * Time.deltaTime;

        //Hut
        if (StoredWood >= 2)
        {
            Hut.SetActive(true);
        }
        if (StoredWood >= HutCost)
        {
            PurchaseHutButton.interactable = true;
        }
        else PurchaseHutButton.interactable = false;

        //Kittens
        KittensText.text = FreeKittens.ToString() + "/" + AllKittens.ToString();

        //Woodcutters
        if( FreeKittens < 1)
        {
            AddWoodcutButton.interactable = false;
            AddAllWoodcutButton.interactable = false;
        }
        else
        {
            AddWoodcutButton.interactable = true;
            AddAllWoodcutButton.interactable = true;
        }

        if( AssignedWoodcut < 1)
        {
            RemoveWoodcutButton.interactable= false;
            RemoveAllWoodcutButton.interactable = false;
        }
        else
        {
            RemoveWoodcutButton.interactable = true;
            RemoveAllWoodcutButton.interactable = true;
        }

        AssignedWoodcutText.text = AssignedWoodcut.ToString();
        


    }
    public void GatherCatnip()
    {
        if (CatnipResourceBar.activeSelf ==false)
        {
            CatnipResourceBar.SetActive(true);
            StoredCatnip++;
        }
        else
        {
            StoredCatnip += GatherCatnipValue;
            if(StoredCatnip >= 3 && CatnipField.activeSelf==false) 
            {
            CatnipField.SetActive(true);
            }
        }
    }

    public void PurchaseCatnipField()
    {
        SellCatnipFieldButton.interactable=true;
        StoredCatnip -= CatnipFieldCost;
        CatnipIncome += 0.63f;
        CatnipFieldCost *= 1.12f;
        CatnipFieldNumber++;
        CatnipFieldText.text = "Catnip Field (" + CatnipFieldNumber.ToString() + ")";
    }
    public void SellCatnipField()
    {
        CatnipIncome -= 0.63f;
        CatnipFieldCost /= 1.12f;
        CatnipFieldNumber--;
        StoredCatnip += CatnipFieldCost / 2;
        if(CatnipFieldNumber < 1)
        {
            SellCatnipFieldButton.interactable = false;
            CatnipFieldText.text = "Catnip Field";
        }
        else
        {
            CatnipFieldText.text = "Catnip Field (" + CatnipFieldNumber.ToString() + ")";
        }
    }

    public void RefineCatnip()
    {
        WoodResourceBar.SetActive(true);
        StoredWood += 1;
        StoredCatnip -= 100;
    }

    public void PurchaseHut()
    {
        SellHutButton.interactable = true;
        HutNumber++;
        HutText.text = "Hut (" + HutNumber.ToString() + ")";
        if(AllKittens<HutNumber*2)
        {
            FreeKittens += 2;
            AllKittens += 2;
        }
        StoredWood -= HutCost;
        HutCost *= 2.5f;
        VillageBtn.SetActive(true);
    }

    public void SellHut()
    {
        HutNumber--;
        HutCost /= 2.5f;
        StoredWood += HutCost / 2;
        if(HutNumber < 1) 
        {
            SellHutButton.interactable= false;
            HutText.text = "Hut";
        }
        else
        {
            HutText.text= "Hut (" + HutNumber.ToString() + ")";
        }
    }


    public void VillageButton()
    {
        Bonfire.SetActive(false);
        Village.SetActive(true);
    }

    public void BonfireButton()
    {
        Bonfire.SetActive(true);
        Village.SetActive(false);
    }

    public void AddWoodcut()
    {
        FreeKittens--;
        AssignedWoodcut++;
    }

    public void RemoveWoodcut()
    {
        FreeKittens++;
        AssignedWoodcut --;
    }

    public void AddAllWoodcut()
    {
        AssignedWoodcut+= FreeKittens;
        FreeKittens = 0;       
    }

    public void RemoveAllWoodcut() 
    {
        FreeKittens += AssignedWoodcut;
        AssignedWoodcut = 0;
    }

    public void ClearAllJobs()
    {
        RemoveAllWoodcut();
    }
}
